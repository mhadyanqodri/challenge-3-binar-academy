import { StyleSheet, Text, View, Dimensions } from 'react-native'
import React from 'react'

const Synopsis = ({synopsis}) => {
    return (
        <View style={styles.container}>
            <Text style={styles.synopsis}>{synopsis}</Text>
        </View>
    )
}

export default Synopsis

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        width: windowWidth*0.9,
        alignItems: 'center',
        marginHorizontal: 20,
    },
    synopsis: {
        color: 'white'
    }
})