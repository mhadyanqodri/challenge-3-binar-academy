import { StyleSheet, Text, View, TouchableOpacity, Dimensions } from 'react-native'
import React from 'react'

const CardGenre = ({ genre }) => {
    return (
        <View style={styles.genreContainer}>
            <TouchableOpacity>
                <Text style={styles.genreLabel}>{genre}</Text>
            </TouchableOpacity>
        </View>
    )
}

export default CardGenre

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    genreContainer: {
        backgroundColor: '#488FB1',
        justifyContent: 'center',
        alignItems: 'center',
        width: windowWidth * 0.225,
        height: windowHeight * 0.03,
        borderRadius: 20,
        marginBottom: 7,
        marginLeft: 5
    },
    genreLabel: {
        color: 'white',
        fontSize: 12
    }
})