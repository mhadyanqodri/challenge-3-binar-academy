import { StyleSheet, Text, View, Dimensions, Image, ActivityIndicator } from 'react-native'
import React, { useState, useEffect } from 'react'
import ButtonFilm from '../ButtonFilm'
import { CardGenre } from "../../component";
import axios from 'axios';

const CardFilm = ({ title, date, rating, source, onPress, genre }) => {

    const [isLoading, setLoading] = useState(true);

    const [data, setData] = useState([]);

    useEffect(() => {
        getData(genre);
    }, []);

    async function getData(genre) {
        try {
            await axios.get(`http://code.aldipee.com/api/v1/movies/${genre}`).then((response) => {
                setData(response.data);
                console.log(response.data)
            });
        } catch (error) {
            console.error(error);

        } finally {
            setLoading(false)
        }
    }

    return (
        <>
            {isLoading ? <ActivityIndicator /> : (
                <View style={styles.borderVertical}>
                    <Image source={source} style={styles.latestFilm}></Image>
                    <View style={styles.descriptionsWrapper}>
                        <Text style={styles.judul}>{title}</Text>
                        <Text style={styles.releaseDate}>{date}</Text>
                        <Text style={styles.rating}>{rating}</Text>
                        <View style={styles.genreWrapper}>
                            {data.genres.map((item) => {
                                return <CardGenre key={item.id} genre={item.name} />
                            })}
                        </View>
                        <View style={styles.buttonWrapper}>
                            <ButtonFilm onPress={onPress} button={'Show More'} />
                        </View>
                    </View>
                </View>
            )}
        </>
    )
}

export default CardFilm

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    borderVertical: {
        paddingVertical: 10,
        flexDirection: 'row',
        width: windowWidth*0.95,
        height: windowHeight*0.35
    },
    latestFilm: {
        width: null,
        height: null,
        borderRadius: 10,
        marginTop: -5,
        flex:3.4
    },
    descriptionsWrapper: {
        marginLeft: 15,
        marginTop: -2,
        flex:4
    },
    judul: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold'
    },
    releaseDate: {
        fontSize: 15,
        color: 'white'
    },
    rating: {
        fontSize: 15,
        color: 'gold'
    },
    genreWrapper: {
        marginTop: 5,
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginLeft: -5
    },
    buttonWrapper: {
        marginTop: 5
    }
})