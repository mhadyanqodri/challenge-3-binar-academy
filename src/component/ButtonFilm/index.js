import { StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native'
import React from 'react'

const ButtonFilm = ({button, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.buttonMore}>
                <Text style={styles.buttonLabel}>{button}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default ButtonFilm

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    buttonMore: {
        backgroundColor: '#FFC300',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        width: windowWidth*0.35,
        height: windowHeight*0.049,
        borderWidth: 1
    },
    buttonLabel: {
        color: 'white',
    }
})