import { StyleSheet, View, TouchableOpacity, Image, Dimensions } from 'react-native'
import React from 'react'

const CardRecomended = ({source, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.borderHorizontal}>
                <Image source={source} style={styles.recomendedFilm}></Image>
            </View>
        </TouchableOpacity>
    )
}

export default CardRecomended

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    borderHorizontal: {
        paddingHorizontal: windowWidth * 0.02,
    },
    recomendedFilm: {
        width: windowWidth * 0.39,
        height: windowHeight * 0.33,
        borderRadius: 10
    }
})