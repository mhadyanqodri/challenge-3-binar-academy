import { StyleSheet, View, Dimensions, TouchableOpacity } from 'react-native'
import React from 'react'
import {Love, LoveActive} from '../../assets';

const LoveButton = ({onPress, onLongPress, Title}) => {
    function Icon() {
        if (Title == 'love') return onPress ? <LoveActive/> : <Love/>

        return <Love />;
    }

    return (
        <TouchableOpacity onPress={onPress} onLongPress={onLongPress}>
            <View style={styles.buttonWrapper}>
                <Icon />
            </View>
        </TouchableOpacity>
    )
}

export default LoveButton

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({})