import { StyleSheet, View, TouchableOpacity, Dimensions } from 'react-native'
import React from 'react'
import { Berbagi } from '../../assets';


const ShareButton = ({ onPress }) => {
    function Icon({ title }) {
        if (title == 'share') return <Berbagi />;

        return <Berbagi />;
    }

    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.buttonWrapper}>
                <Icon />
            </View>
        </TouchableOpacity>
    )
}

export default ShareButton

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({})