import { StyleSheet, Text, View, Dimensions, Image } from 'react-native'
import React from 'react'

const CardDescription = ({ judul, rating, status, runtime, tagline, source, date }) => {
  return (
    <View style={styles.description}>
      <Image source={source} style={styles.imagePoster}></Image>
      <View style={styles.descriptionsWrapper}>
        <Text style={styles.judul}>{judul}</Text>
        <Text style={styles.rating}>{rating}</Text>
        <View style={styles.wrapper}>
          <Text style={styles.label}>{status}</Text>
          <Text style={styles.label}>{runtime} Menit</Text>
          <Text style={styles.label}>{tagline}</Text>
          <Text style={styles.date}>{date}</Text>
        </View>
      </View>
    </View>
  )
}

export default CardDescription

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  description: {
    backgroundColor: '#3E497A',
    height: windowHeight * 0.25,
    marginHorizontal: windowWidth * 0.05,
    borderRadius: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 15,
    marginTop: -windowHeight * 0.12,
    flexDirection: 'row',
  },
  imagePoster: {
    height: windowHeight * 0.2,
    width: windowWidth * 0.3,
    marginTop: 20,
    marginLeft: 15,
    borderRadius: 8
  },
  descriptionsWrapper: {
    marginLeft: 15,
    marginTop: 20,
    flex: 1
  },
  judul: {
    color: 'white',
    fontSize: 18,
    fontWeight: '900',
  },
  rating: {
    fontSize: 16,
    color: 'gold',
    fontWeight: '700'
  },
  label: {
    color: 'white',
    fontSize: 14
  },
  date: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 15
  },
  wrapper: {
    marginTop: 10
  }
})