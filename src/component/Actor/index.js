import { StyleSheet, Text, View, Image, Dimensions } from 'react-native'
import React from 'react'

const Actor = ({name, source}) => {
    return (
        <View style={styles.container}>
            <Image source={source} style={styles.actorImage}></Image>
            <View style={styles.actorWrapper}>
                <Text style={styles.actorName}>{name}</Text>
            </View>
        </View>
    )
}

export default Actor

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#3E497A',
        height: windowHeight * 0.25,
        width: windowWidth * 0.25,
        borderRadius: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
        marginBottom: 20,
        alignItems: 'center',
        marginLeft: 23
    },
    actorImage: {
        height: windowHeight * 0.2,
        width: windowWidth * 0.25,
        borderRadius: 10
    },
    actorName: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 12,
        marginTop: 8
    }
})