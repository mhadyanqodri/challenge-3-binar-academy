import {
  StyleSheet, View, Dimensions, TouchableOpacity,
} from 'react-native';
import React from 'react';
import {Arrow} from '../../assets';

function BackButton({onPress}) {
  function Icon({title}) {
    if (title == 'back') return <Arrow />;

    return <Arrow/>;
  }

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.buttonWrapper}>
        <Icon />
      </View>
    </TouchableOpacity>
  );
}

export default BackButton;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
});
