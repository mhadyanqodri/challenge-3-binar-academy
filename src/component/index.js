import CardRecomended from './CardRecomended';
import CardFilm from './CardFilm';
import CardGenre from './CardGenre';
import ButtonFilm from './ButtonFilm';
import CardDescription from './CardDescription';
import Synopsis from './Synopsis';
import Actor from './Actor';
import BackButton from './BackButton';
import ShareButton from "./ShareButton";
import LoveButton from "./LoveButton";
import InternetOffline from "./InternetOffline";

export {
  CardRecomended,
  CardFilm,
  CardGenre,
  ButtonFilm,
  CardDescription,
  Synopsis,
  Actor,
  BackButton,
  ShareButton,
  LoveButton,
  InternetOffline,
};
