import Star from './Star.svg';
import StarOff from './StarOff.svg';
import Arrow from './Arrow.svg';
import Love from './love.svg';
import Berbagi from './Berbagi.svg';
import LoveActive from './loveActive.svg';

export {
  Star,
  StarOff,
  Arrow,
  Love,
  Berbagi,
  LoveActive,
};
