import ryanReynolds from './ryanReynolds.jpg';
import walkerScobell from './walkerScobell.jpg';
import zoeSaldana from './zoeSaldana.jpg';
import jeniniferGarner from './jeniniferGarner.jpg';
import markRuffalo from './markRuffalo.jpg';

export {
  ryanReynolds,
  walkerScobell,
  zoeSaldana,
  jeniniferGarner,
  markRuffalo,
};
