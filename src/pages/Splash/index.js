import { StyleSheet, Text, View, ImageBackground } from 'react-native'
import React, {useEffect} from 'react'
import { SplashLogo } from '../../assets'

const Splash = ({ navigation }) => {

    useEffect(() => {
      setTimeout( () => {
        navigation.replace('MainApp');
      }, 2500)
    }, [navigation]);
  
    return (
        <View style={styles.container}>
            <ImageBackground source={SplashLogo} style={styles.logo}></ImageBackground>
            <Text style={styles.aplicationName}>Movie App</Text>
            <Text style={styles.creator}>M Hadyan Qodri</Text>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#21325E'
    },
    logo: {
        width: 80,
        height: 80
    },
    aplicationName: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold'
    },
    creator: {
        fontSize: 13,
        position: 'absolute',
        bottom: 18,
        color: 'white'
    }
})