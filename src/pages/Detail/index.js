import { 
  StyleSheet, 
  Text, 
  View, 
  Dimensions, 
  ImageBackground, 
  ActivityIndicator, 
  Share 
} from 'react-native'
import React, { useState, useEffect } from 'react'
import { ScrollView } from 'react-native-gesture-handler';
import { 
  CardDescription, 
  Synopsis, 
  Actor, 
  CardGenre, 
  BackButton, 
  ShareButton, 
  LoveButton 
} from "../../component";
import axios from 'axios';
import Home from '../Home';

const Detail = ({ route, navigation }) => {
  const [isLoading, setLoading] = useState(true);
  const { id } = route.params;
  const [data, setData] = useState([]);

  const onShare = async () => {
    try {
      const result = await Share.share({
        message:
          `go to home for detail movies ${data.homepage}`,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  useEffect(() => {
    getData(id);
  }, []);

  async function getData(id) {
    try {
      await axios.get(`http://code.aldipee.com/api/v1/movies/${id}`).then((response) => {
        setData(response.data);
      });
    } catch (error) {
      console.error(error);

    } finally {
      setLoading(false)
    }
  }


  return (
    <>
      {isLoading ? <ActivityIndicator></ActivityIndicator> : (
        <View style={styles.page}>
          <ScrollView>
            <View>
              <ImageBackground source={{uri : data.backdrop_path}} style={styles.header}>
                <View style={styles.buttonBack}>
                  <BackButton title="back" onPress={() => navigation.navigate(Home)}/>
                </View>
                <View style={styles.buttonShare}>
                  <ShareButton title="share" onPress={onShare}/>
                </View>
                <View style={styles.buttonLove}>
                  <LoveButton title="love"/>
                </View>
              </ImageBackground>
            </View>
            <View>
              <CardDescription source={{ uri: data.poster_path }} judul={data.original_title} rating={data.vote_average} status={data.status} runtime={data.runtime} tagline={data.tagline}  date={data.release_date}/>
            </View>
            <View>
              <Text style={styles.label}>Genre</Text>
              <View style={styles.genreWrapper}>
                {data.genres.map((item) => {
                  return <CardGenre key={item.id} genre={item.name} />
                })}
              </View>
            </View>
            <View>
              <Text style={styles.label}>Synopsis</Text>
              <View>
                <Synopsis synopsis={data.overview} />
              </View>
            </View>
            <View style={styles.actor}>
              <Text style={styles.label}>Actors</Text>
              <View style={styles.actorWrapper}>
                {data.credits.cast.map((item) => {
                  return <Actor key={item.id} source={{ uri: item.profile_path }} name={item.original_name} />
                })}
              </View>
            </View>
          </ScrollView>
        </View>
      )}

    </>
  )
}

export default Detail

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#21325E',
  },
  label: {
    fontSize: 22,
    fontWeight: 'bold',
    color: 'white',
    marginBottom: 8,
    marginVertical: windowHeight * 0.01,
    marginHorizontal: windowWidth * 0.06
  },
  genreWrapper: {
    marginTop: 5,
    flexDirection: 'row',
    marginLeft: windowWidth * 0.02,
  },
  actor: {
    backgroundColor: '#7882A4',
    marginTop: 20,
    borderTopStartRadius: 25,
    borderTopEndRadius: 25
  },
  actorWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
  },
  buttonWrapper: {
    borderRadius: 40,
    width: 40,
    height: 40,
    backgroundColor: 'white',
  },
  buttonBack: {
    marginTop: 15,
    marginLeft: 15,
  },
  buttonShare: {
    marginTop: -windowHeight*0.015,
    marginHorizontal: windowWidth*0.82,
  },
  buttonLove: {
    marginLeft: windowWidth*0.92,
    marginTop: -windowHeight*0.0125
  }
})