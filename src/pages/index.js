import Splash from './Splash';
import Home from './Home';
import Detail from './Detail';

export { Home, Detail, Splash };
