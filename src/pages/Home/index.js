import { StyleSheet, Text, View, Dimensions, ActivityIndicator } from 'react-native'
import React, { useState, useEffect } from 'react'
import { ScrollView } from 'react-native-gesture-handler'
import { CardRecomended, CardFilm } from "../../component";
import axios from 'axios';

const Home = ({ navigation }) => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    useEffect(() => {
        getData();
    }, []);

    async function getData() {
        try {
            await axios.get('http://code.aldipee.com/api/v1/movies').then((response) => {
                setData(response.data.results);
                console.log(response.data.results)
            });
        } catch (err) {
            console.error(err);

        } finally {
            setLoading(false)
        }
    }
    return (
        <>
            {isLoading ? <ActivityIndicator /> : (
                <View style={styles.page}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <Text style={styles.label}>Recomended</Text>
                        <View>
                            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                <View style={styles.recomended}>
                                    {data.map((item) => {
                                        return <CardRecomended key={item.id} source={{ uri: item.poster_path }} onPress={() => navigation.navigate('Detail', { id: item.id })} />
                                    })}
                                </View>
                            </ScrollView>
                        </View>
                        <Text style={styles.label}>Latest Upload</Text>
                        <View>
                            <View style={styles.latestUpload}>
                                {data.map((item) => {
                                    return <CardFilm key={item.id} title={item.title} rating={item.vote_average} date={item.release_date} source={{ uri: item.poster_path }} onPress={() => navigation.navigate('Detail', { id: item.id })} genre={item.id} />
                                })}
                            </View>
                        </View>
                    </ScrollView>
                </View>
            )}

        </>
    )

}

export default Home

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: '#21325E'
    },
    recomended: {
        flexDirection: 'row',
        marginHorizontal: windowWidth * 0.03
    },
    label: {
        fontSize: 22,
        fontWeight: 'bold',
        color: 'white',
        marginBottom: 8,
        marginVertical: windowHeight * 0.03,
        marginHorizontal: windowWidth * 0.05
    },
    latestUpload: {
        marginHorizontal: windowWidth * 0.05
    },
    genreWrapper: {
        marginTop: 5,
        flexDirection: 'row',
        marginLeft: windowWidth * 0.05,
    }
})