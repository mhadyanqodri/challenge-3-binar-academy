import { StyleSheet } from 'react-native'
import React, {useEffect, useState} from 'react'
import { NavigationContainer } from '@react-navigation/native';
import Router from './router';
import NetInfo from "@react-native-community/netinfo";
import { InternetOffline } from "../src/component";

const App = () => {
  const [isOffline, setOfflineStatus] = useState(false);
  
  useEffect(() => {
    const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setOfflineStatus(offline);
    });
  
    return () => removeNetInfoSubscription();
  }, []);
  return (
    <NavigationContainer>
      {isOffline ? <InternetOffline/> :  <Router />}
    </NavigationContainer>
  )
}

export default App

const styles = StyleSheet.create({})